# Setup Project

## Partie 1, La BDD :
Dans le dossier "BDD" il y a la copie de ma BDD pour faire marcher le projet, il suffit juste de l'importer en vérifiant bien son nom "rsocial".

## Partie 2, La Configuration :

Configurer "hibernate.cfg.xml" pour la connexion à la BDD, par défaut : "localhost:3306" en MySQL

## Partie 3, Liferay :

Télécharger Liferay Portal CE : [> ICI <](https://www.liferay.com/fr/downloads-community)
Ensuite importer le sur Eclipse et tout autre type d'IDE compatible.

## Partie 4, Lancement :
Par défaut, il y aura un compte de "créer" dans la BDD (Voir partie Documentation), donc une fois votre compte Liferay créer, vous devriez apercevoir des Question ayant pour auteur "Bastien Boubon".
Si ce n'ai pas le cas et qu'il y a une erreur, merci de me contactez par mail. 
