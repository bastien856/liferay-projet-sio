package com.gsb.hib;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


  
public class HibernateUtil {
  
    private static final SessionFactory sessionFactory = buildSessionFactory();
  
    private static SessionFactory buildSessionFactory() {
    	try {
    	Configuration config = new Configuration().configure();
    	config.setProperty("javax.persistence.validation.mode", "NONE");
		return config.buildSessionFactory();
    	} catch (Throwable ex) {
    		System.err.println("Initial SessionFactory creation Failed :" + ex);
    		throw new ExceptionInInitializerError(ex);
    	}
		
    }
  
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
  
    public static void shutdown() {
        // Close caches and connection pools
        getSessionFactory().close();
    }
  
}
