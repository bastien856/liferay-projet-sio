package com.gsb.dao;

import java.util.List;

import javax.persistence.Cache;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.mapping.Map;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.persister.collection.AbstractCollectionPersister;
import org.hibernate.persister.entity.EntityPersister;

import com.gsb.hib.HibernateUtil;
import com.gsb.object.Post;

public class PostDAO {
	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	

	public Post findById(int id) {
		Session session = sessionFactory.openSession();
		Post post = (Post) session.get(Post.class, id);
		session.close();
		return post;
	}

	public void save(Post post) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(post);
		session.getTransaction().commit();
		session.close();

	}

	public Post update(Post post) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.merge(post);
		session.getTransaction().commit();
		session.close();
		return post;
	}

	public void delete(Post post) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.delete(post);
		session.getTransaction().commit();
		session.close();
	}

	public List<Post> findAll() {
		Session session = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		List<Post> posts = session.createQuery("from Post").list();
		session.close();
		return posts;
	}
	

}
