package com.gsb.object;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Reponse")
public class Reponse {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="auteur")
	private String auteur;
	
	@Column(name="titre")
	private String titre;
	
	@Column(name="description")
	private String description;
	
	@Column(name="date")
	private Date date;
	

	@Column(name = "id")
	private int id_user;

	@Column(name = "idpost")
	private int id_post;
	
	public Reponse() {}
	public Reponse(String auteur, String titre, String description, Date date){
		
		this.auteur = auteur;
		this.titre = titre;
		this.description = description;
		this.date = date;
		
	}


	//GETTER
	public int getId() {
		return id;
	}
	
	public String getAuteur() {
		return auteur;
	}
	
	public String getTitre() {
		return titre;
	}
	
	public String getDescription() {
		return description;
	}
	
	public Date getDate() {
		return date;
	}
	
	public int getIdUser() {
		return id_user;
	}
	
	public int getIdPost() {
		return id_post;
	}
	
	//SETTER

	public void setId(int id) {
		this.id = id;
	}
	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public void setIdUser(int id_user) {
		this.id_user = id_user;
	}
	
	public void setIdPost(int id_post) {
		this.id_post = id_post;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
