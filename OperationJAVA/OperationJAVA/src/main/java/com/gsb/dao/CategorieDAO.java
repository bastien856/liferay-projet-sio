package com.gsb.dao;

import java.util.List;

import javax.persistence.Cache;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.gsb.hib.HibernateUtil;
import com.gsb.object.Categorie;

public class CategorieDAO {
	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	
	
	public Categorie findById(int id) {
		Session session = sessionFactory.openSession();
		Categorie categorie = (Categorie) session.get(Categorie.class, id);
		session.close();
		return categorie;
	}
	
	public void save(Categorie categorie) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(categorie);
		session.getTransaction().commit();
		session.close();
		
	}
	
	public Categorie update(Categorie categorie) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.merge(categorie);
		session.getTransaction().commit();
		session.close();
		return categorie;
	}
	
	public void delete(Categorie categorie) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.delete(categorie);
		session.getTransaction().commit();
		session.close();
	}
	
	public List<Categorie> findAll(){
		Session session = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		List<Categorie> categories = session.createQuery("from Categorie").list();
		session.close();
		return categories;
	}
	
}
