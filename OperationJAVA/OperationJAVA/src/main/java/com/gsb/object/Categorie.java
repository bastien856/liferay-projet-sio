package com.gsb.object;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "Categorie")
public class Categorie {
	
	@Id  
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name = "titre")
	private String titre;
	
	@Column(name = "description")
	private String description;
	
	public Categorie() {}
	public Categorie(String titre, String description) {
		this.titre = titre;
		this.description = description;
	}
	
	//GETTER
	public int getId() {
		return this.id;
	}
	
	public String getTitre() {
		return titre;
	}
	
	public String getDescription() {
		return description;
	}
	//SETTER
	public void setId(int id) {
		this.id = id;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	
	public void setDescription(String desc) {
		this.description = desc;
	}
	
}
