package com.portlet;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.component.visit.VisitCallback;
import javax.faces.component.visit.VisitContext;
import javax.faces.component.visit.VisitResult;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.gsb.dao.AccountDAO;
import com.gsb.dao.CategorieDAO;
import com.gsb.dao.PostDAO;
import com.gsb.object.Account;
import com.gsb.object.Categorie;
import com.gsb.object.Post;
import com.liferay.faces.util.context.FacesContextHelperUtil;

@RequestScoped
@ManagedBean
public class MyPortlet {
	
	private CategorieDAO categorieDao = new CategorieDAO();
	private PostDAO postDAO = new PostDAO();
	private AccountDAO accountDAO = new AccountDAO();
	private String values;
	private String name;

	
	//Formulaire MUR
	private List<Post> allPost;
	
	private int page;
	
	

	
	public void testP() {
		System.out.println("==========================");
	}

	
	public String getName() {
	    return name;
	}

	public void setName(String name) {
	    this.name = name;
	}

	
	public String getValues() {
	    return values;
	}

	public void setValues(String values) {
	    this.values = values;
	}
	

	public void setPage(int p) {
		page = p;
	}
	
	public int getPage() {
		return page;
	}
	
	
	
	
	
	
	//Test pour select dans les catégorie !
	public void submit() {
		FacesContextHelperUtil.addGlobalSuccessInfoMessage();
		this.send();
	    
	}
	
	
	

	
	
	//TEST pour récuperer les valeurs de la table Categorie via l'ID
	public void send() {
		Boolean isOk = false;
		System.out.println("-------------------"+getValues()+"|"+getName());
		List<Categorie> titre = categorieDao.findAll();
		for(Categorie cat : titre) {
			if(cat.getId() == Integer.parseInt(getValues())) {
				this.name = "titre :"+ cat.getTitre()+ "| Description : "+cat.getDescription() + "| value : "+getValues();
				isOk = true;
			}
		}
		if(isOk == false) {
			this.name = "J'ai pas trouvé !";
		}
		System.out.println("-------------------"+getValues()+"|"+getName());
		
	}
	
	public List<Post> getAllPost() {
		allPost = postDAO.findAll();
		return allPost;
	}
	
	public Post getByUser(int id) {
		return postDAO.findById(id);
	}
	
	public String getIMGAccount(int id) {
		String url;
		if(accountDAO.findById(id).getUrlPhoto() != null) {
			url = "http://localhost:8080/image"+accountDAO.findById(id).getUrlPhoto();
		} else {
			url = "https://image.flaticon.com/icons/png/512/37/37171.png";
		}
		return url;
		
	}
	
	public String getNomPrenom(int id) {
		String nomPrenom = "";
		if(accountDAO.findById(id).getPrenom() != null) {
			if(accountDAO.findById(id).getNom()!= null) {
				nomPrenom = accountDAO.findById(id).getPrenom() + " " + accountDAO.findById(id).getNom();
			}
		} else {
			nomPrenom = "Inconnu !";
		}
		return nomPrenom;
	}
	

	
	
	public void selectPost(int page) {
		UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
		UIComponent component = viewRoot.findComponent("userPost");
		component.getAttributes().put("value", "#{myPortlet.getUserForPost("+page+")}");
		
	}
	
	
	
	
	
	
}