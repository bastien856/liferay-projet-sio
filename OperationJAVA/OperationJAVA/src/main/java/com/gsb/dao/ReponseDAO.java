package com.gsb.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.gsb.hib.HibernateUtil;
import com.gsb.object.Reponse;

public class ReponseDAO {
	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	
	public Reponse findById(int id) {
		Session session = sessionFactory.openSession();
		Reponse reponse = (Reponse) session.get(Reponse.class, id);
		session.close();
		return reponse;
	}
	
	public void save(Reponse reponse) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(reponse);
		session.getTransaction().commit();
		session.close();
		
		
	}
	
	public Reponse update(Reponse reponse) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.merge(reponse);
		session.getTransaction().commit();
		session.close();
		return reponse;
	}
	
	public void delete(Reponse reponse) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.delete(reponse);
		session.getTransaction().commit();
		session.close();
	}
	
	public List<Reponse> findAll(){
		Session session = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		List<Reponse> reponses = session.createQuery("from Reponse").list();
		session.close();
		return reponses;
	}
	
}
