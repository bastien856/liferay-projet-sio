package com.gsb.object;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "account")
public class Account {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "idLiferay")
	private int idLiferay;

	@Column(name = "pseudo")
	private String pseudo;
	
	@Column(name = "pass")
	private String pass;
	
	@Column(name = "nom")
	private String nom;
	
	@Column(name = "prenom")
	private String prenom;
	
	@Column(name = "mail")
	private String mail;
	
	@Column(name = "dateNaissance")
	private Date dateNaissance;
	
	@Column(name = "dateNaissance")
	private String urlPhoto;
	
	@Column(name = "rang")
	private String rang;

	
	public Account() {}
	public Account(String pseudo, String pass, String nom, String prenom, String mail, Date dateNaissance,
			String urlPhoto, String rang) {
		this.pseudo = pseudo;
		this.pass = pass;
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.dateNaissance = dateNaissance;
		this.urlPhoto = urlPhoto;
		this.rang = rang;
	}

	// GETTER
	public int getId() {
		return this.id;
	}
	
	public int getIdLiferay() {
		return this.idLiferay;
	}

	public String getPseudo() {
		return pseudo;
	}

	public String getPass() {
		return pass;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getMail() {
		return mail;
	}

	public Date getDateNaissance() {
		return dateNaissance;
	}

	public String getUrlPhoto() {
		return urlPhoto;
	}

	public String getRang() {
		return rang;
	}

	// SETTER
	public void setId(int id) {
		this.id = id;
	}
	
	public void setIdLiferay(int idLiferay) {
		this.idLiferay = idLiferay;
	}

	public void setPseudo(String Pseudo) {
		pseudo = Pseudo;
	}

	public void setPass(String Pass) {
		pass = Pass;
	}

	public void setNom(String Nom) {
		nom = Nom;
	}

	public void setPrenom(String Prenom) {
		prenom = Prenom;
	}

	public void setMail(String Mail) {
		mail = Mail;
	}

	public void setDateNaissance(Date Date) {
		dateNaissance = Date;
	}

	public void setUrlPhoto(String UrlPhoto) {
		urlPhoto = UrlPhoto;
	}

	public void setRang(String Rang) {
		rang = Rang;
	}
}
