package com.gsb.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.gsb.hib.HibernateUtil;
import com.gsb.object.Account;

public class AccountDAO {
	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	
	public Account findById(int id) {
		Session session = sessionFactory.openSession();
		Account account = (Account) session.get(Account.class, id);
		session.close();
		return account;
	}
	
	public void save(Account account) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(account);
		session.getTransaction().commit();
		session.close();
		
	}
	
	public Account update(Account account) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.merge(account);
		session.getTransaction().commit();
		session.close();
		return account;
	}
	
	public void delete(Account account) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.delete(account);
		session.getTransaction().commit();
		session.close();
	}
	
	public List<Account> findAll(){
		Session session = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		List<Account> accounts = session.createQuery("from Account").list();
		session.close();
		return accounts;
	}
	
}
