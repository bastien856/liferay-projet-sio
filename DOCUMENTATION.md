
# Documentation
Elle n'est pas définitif, elle évoluera au fil du temps.

## Base de donnée :
La BDD va changer, car elle n'est pas 100% fonctionnel et terminer.
Elle est constituer de 5 tableaux de données :
 - **account** : Récupère les infos utilise du compte Liferay.
 - **categorie** : Liste de catégorie de question.
 - **notification** : Liste de type de notification.
 - **post** : Liste des questions posé.
 - **reponse** : Liste des réponses liés au question.

### Table Account :

 - **id** : L'id du compte.
 - **idLiferay** : L'id du compte Liferay.
 - **pseudo** : Nom prénom du compte Liferay.
 - **pass** : Plus utilisés car on passe par Liferay désormais.
 - **nom** : Plus utilisés car on passe par Liferay désormais.
 - **prenom** : Plus utilisés car on passe par Liferay désormais.
 - **mail** : Mail du compte Liferay.
 - **urlPhoto** : Photo de profil du compte Liferay.
 - **rang** : Plus utilisés car on passe par Liferay désormais.

### Table Categorie :

 - **id** : L'id de la catégorie.
 - **titre** : La dénomination de la catégorie.
 - **description** : La description de la catégorie.

### Table Notification :

 - **id** : L'id de la notification.
 - **type** : Le type de notification (Urgent, Avertissement, ...).
 - **titre** : La dénomination de la notification.
 - **description** : La description de la notification.

### Table Post :

 - **id** : L'id du post.
 - **titre** : La dénomination du post.
 - **contenu** : Le contenu du post.
 - **auteur** : L'auteur du post.
 - **date** : La date du post.
 - **tag** : Les tag du post.
 - **id_user** : L'id du compte lié au post.
 - **id_categorie** : L'id de la catégorie lié au post.


### Table Reponse :

 - **id** : L'id de la réponse.
 - **auteur** : L'auteur de la réponse.
 - **titre** : Le titre de la réponse.
 - **description** : La description de la question.
 -  **date** : La date de la réponse.
 - **id_user** : L'id du compte lié à la réponse.
 - **id_post** : L'id du post lié à la réponse.

## Fonctionnement du site :

*(Cette partie concerne les fonctions du site au moment ou j'update la documentation !)*

Au moment de l'arrivée sur le site, vous allez être redirigés vers la page de connexion (si vous n'êtes pas connecté).  
Une fois identifier, vous avez accès à la liste de toutes les questions posées jusqu'à maintenant, il vous suffit alors de cliquer sur la question que vous voulez et vous aurez le choix d'y répondre ou non.  
Sur la page des questions présente, vous aurez un bouton pour créer une question, (au moment ou j'écris cette partie là, la partie question n'a aucun CSS / mise en forme de la page).