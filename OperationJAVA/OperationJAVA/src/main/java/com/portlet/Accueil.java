package com.portlet;

import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.primefaces.PrimeFaces;

import com.gsb.dao.AccountDAO;
import com.gsb.object.Account;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.LiferayPortletContext;
import com.liferay.portal.kernel.security.auth.PrincipalThreadLocal;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;

@ManagedBean
@RequestScoped
public class Accueil {
	
	private User user;
	private AccountDAO accountDAO = new AccountDAO();
	
	
	public boolean canConnect() throws PortalException {
		boolean result = false;
		
		String mail = getCurrentUser().getEmailAddress();
		System.out.println(","+mail+",");
		if(mail != null && mail != "" && !mail.equalsIgnoreCase("default@liferay.com")) {
			result = true;
		}
		
		return result;
	}
	
	public void isConnect() throws PortalException{
		if(canConnect()) {
			PrimeFaces.current().executeScript("login();");
			profileExistOnBDD();
		} else {
			PrimeFaces.current().executeScript("document.location.href='http://localhost:8080/c/portal/login';");
		}
		
	}
	
	public void profileExistOnBDD() throws PortalException {
		User tUser = user;
		ThemeDisplay theme = new ThemeDisplay();
		theme.setUser(tUser);
		int id = (int) getUserID();
		List<Account> accList = accountDAO.findAll();
		boolean accountOk = false;
		for(Account acc : accList) {
			if(acc.getIdLiferay() == id) {
				accountOk = true;
			}
		}
		if (!accountOk) {
			System.out.println("ACCOUNT NUL");
			Account acc = new Account();
			acc.setId(id);
			acc.setIdLiferay(id);
			acc.setMail(user.getEmailAddress());
			acc.setNom(user.getLastName());
			acc.setPrenom(user.getFirstName());
			acc.setPass("NONE");
			acc.setDateNaissance(new Date());
			acc.setPseudo(user.getFullName());
			acc.setRang(user.getGroup().getName());
			acc.setUrlPhoto(theme.getUser().getPortraitURL(theme));
			
			accountDAO.save(acc);
		} else {
			System.out.println("ACCOUNT OK");
		}
	}
	
	public int getUserIdNormal() {
		List<Account> accList = accountDAO.findAll();
		boolean accountOk = false;
		for(Account acc : accList) {
			if(acc.getIdLiferay() == getUserID()) {
				return acc.getId();
			}
		}
		
		return 0;
	}
	
	
	
	
	
	
	
	
	public User getCurrentUser() throws PortalException {
		user = UserLocalServiceUtil.getUser(PrincipalThreadLocal.getUserId());
		return user;
	}
	
	public long getUserID() {
		long id = PrincipalThreadLocal.getUserId();
		return id;
	}
	
	
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
}
