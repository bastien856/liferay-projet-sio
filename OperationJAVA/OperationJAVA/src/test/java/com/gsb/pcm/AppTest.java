package com.gsb.pcm;

import java.util.Date;
import java.util.List;

import org.junit.Assert;

import com.gsb.dao.AccountDAO;
import com.gsb.dao.CategorieDAO;
import com.gsb.dao.NotificationDAO;
import com.gsb.dao.PostDAO;
import com.gsb.dao.ReponseDAO;
import com.gsb.object.Account;
import com.gsb.object.Categorie;
import com.gsb.object.Notification;
import com.gsb.object.Post;
import com.gsb.object.Reponse;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {
	AccountDAO accountDao = new AccountDAO();
	CategorieDAO categorieDao = new CategorieDAO();
	NotificationDAO notifDao = new NotificationDAO();
	PostDAO postDao = new PostDAO();
	ReponseDAO reponseDao = new ReponseDAO();
	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public AppTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(AppTest.class);
	}

	/**
	 * Rigourous Test :-)
	 */

	// TEST POST
	public void testPost() {
		Post post = new Post("titre post", "description post", "Bastien Boubon", new Date(), "tag post");
		post.setIdCategorie(categorieDao.findById(1).getId());
		post.setIdUser(accountDao.findById(1).getId());
		// System.out.println(PostDAO.save(post));
		List<Post> posts = postDao.findAll();
		System.out.println("======TEST POST=======");
		int i = 0;
		for (Post p : posts) {
			System.out.println(i + "/" + p.getContenu());
			i++;
		}
		System.out.println("======TEST POST=======");
		Assert.assertEquals(post.getContenu(), posts.get(i - 1).getContenu());
	}

	// TEST ACCOUNT
	public void testAccount() {
		Account user = new Account("inox", "123", "Truc", "Cheloux", "Aucun@gmail.com", new Date(), "none", "Membre");
		// System.out.println(AccountDAO.save(user));

		List<Account> users = accountDao.findAll();
		System.out.println("======TEST ACCOUNT=======");
		int i = 0;
		for (Account a : users) {
			System.out.println(i + "/" + a.getPseudo());
			i++;
		}
		System.out.println("======TEST ACCOUNT=======");
		Assert.assertEquals(user.getPseudo(), users.get(i - 1).getPseudo());
	}

	// TEST Categorie
	public void testCategorie() {
		Categorie cate = new Categorie("le titre", "Categorie qui consiste à parler d'information");
		// System.out.println(CategorieDAO.save(cate));

		List<Categorie> cates = categorieDao.findAll();
		System.out.println("======TEST CATEGORIE=======");
		int i = 0;
		for (Categorie c : cates) {
			System.out.println(i + "/" + c.getTitre());
			i++;
		}
		System.out.println("======TEST CATEGORIE=======");
		Assert.assertEquals(cate.getTitre(), cates.get(i - 1).getTitre());
	}

	// TEST Notification
	public void testNotification() {
		Notification notif = new Notification("Urgent", "Erreur", "Il y a une erreur dans le système !");
		// System.out.println(NotificationDAO.save(notif));

		List<Notification> notifs = notifDao.findAll();
		System.out.println("======TEST NOTIFICATION=======");
		int i = 0;
		for (Notification n : notifs) {
			System.out.println(i + "/" + n.getTitre());
			i++;
		}
		System.out.println("======TEST NOTIFICATION=======");
		Assert.assertEquals(notif.getTitre(), notifs.get(i - 1).getTitre());
	}

	// TEST Reponse
	public void testReponse() {
		Reponse reponse = new Reponse(accountDao.findById(1).getPseudo(), "titre réponse", "la la la la la", new Date());
		reponse.setIdPost(postDao.findById(1).getId());
		reponse.setIdUser(accountDao.findById(1).getId());
		//System.out.println(ReponseDAO.save(reponse));

		List<Reponse> reponses = reponseDao.findAll();
		System.out.println("======TEST Reponse=======");
		int i = 0;
		for (Reponse r : reponses) {
			System.out.println(i + "/" + r.getTitre());
			i++;
		}
		System.out.println("======TEST Reponse=======");
		Assert.assertEquals(reponse.getTitre(), reponses.get(i - 1).getTitre());
	}
}
