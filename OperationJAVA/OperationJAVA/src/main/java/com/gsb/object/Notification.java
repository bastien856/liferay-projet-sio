
package com.gsb.object;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Notification")
public class Notification {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "type")
	private String type;
	
	@Column(name = "titre")
	private String titre;
	
	@Column(name = "description")
	private String description;
	
	
	public Notification() {}
	public Notification(String type, String titre, String description) {
		this.type = type;
		this.titre = titre;
		this.description = description;
	}
	
	//GETTER
	public int getId() {
		return id;
	}
	
	public String getType() {
		return type;
	}
	
	public String getTitre() {
		return titre;
	}
	
	public String getDescription() {
		return description;
	}
	//SETTER
	public void setId(int id) {
		this.id = id;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public void setTitre(String titre) {
		this.titre = titre;
	}
	
	public void setDescription(String desc) {
		this.description = desc;
	}
}
