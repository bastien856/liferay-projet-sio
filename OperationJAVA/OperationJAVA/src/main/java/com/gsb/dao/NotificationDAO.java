package com.gsb.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.gsb.hib.HibernateUtil;
import com.gsb.object.Notification;

public class NotificationDAO {
	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	
	public Notification findById(int id) {
		Session session = sessionFactory.openSession();
		Notification notification = (Notification) session.get(Notification.class, id);
		session.close();
		return notification;
	}
	
	public void save(Notification notification) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(notification);
		session.getTransaction().commit();
		session.close();
		
	}
	
	public Notification update(Notification notification) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.merge(notification);
		session.getTransaction().commit();
		session.close();
		return notification;
	}
	
	public void delete(Notification notification) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.delete(notification);
		session.getTransaction().commit();
		session.close();
	}
	
	public List<Notification> findAll(){
		Session session = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		List<Notification> notifications = session.createQuery("from Notification").list();
		session.close();
		return notifications;
	}
	
}
