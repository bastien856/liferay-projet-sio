package com.portlet;

import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.omg.CORBA.Request;
import org.primefaces.PrimeFaces;

import com.gsb.dao.AccountDAO;
import com.gsb.dao.CategorieDAO;
import com.gsb.dao.PostDAO;
import com.gsb.object.Account;
import com.gsb.object.Post;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.LiferayPortlet;
import com.liferay.portal.kernel.security.auth.PrincipalThreadLocal;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.model.impl.UserImpl;
import com.liferay.portal.util.WebKeys;


@ManagedBean
@RequestScoped
public class Login {
	private CategorieDAO categorieDao = new CategorieDAO();
	private PostDAO postDAO = new PostDAO();
	private AccountDAO accountDAO = new AccountDAO();
	private String pseudo;
	private String password;
	private String errorMessage;
	private String isConnect;
	private int userID;

	// Formulaire Question
	private String tag;
	private String question;

	

	public void sendQuestion(int id) {
		System.out.println("==========================");
		System.out.println(id);
		Account acc = accountDAO.findById(id);
		if (acc != null) {
			if (question != null || tag != null) {
				Post post = new Post();
				post.setAuteur(acc.getPseudo());
				post.setContenu(question);
				post.setDate(new Date());
				post.setIdCategorie(1);
				post.setIdUser(acc.getId());
				post.setTag(tag);
				post.setTitre("Question");

				postDAO.save(post);
				PrimeFaces.current().executeScript("login();");
			}
		}

		System.out.println("==========================");
		System.out.println("Question : " + question);
		System.out.println("TAG : " + tag);
		this.setTag("");
		this.setQuestion("");

		System.out.println("==========================NOP");
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setIsConnect(String isConnect) {
		this.isConnect = isConnect;
	}

	public String getIsConnect() {
		return isConnect;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public int getUserID() {
		return userID;
	}

}
