package com.gsb.object;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "post")
public class Post {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "titre")
	private String titre;
	
	@Column(name = "contenu")
	private String contenu;
	
	@Column(name = "auteur")
	private String auteur;
	
	@Column(name = "date")
	private Date date;
	
	@Column(name = "id")
	private String tag;
	

	@Column(name = "id_user" ) 
	private int idUser;
	
	@Column(name = "id_categorie") 
	private int idCategorie;
	
	public Post() {}
	public Post(String titre, String contenu, String auteur, Date date, String tag) {
		
		this.titre = titre;
		this.contenu = contenu;
		this.auteur = auteur;
		this.date = date;
		this.tag = tag;
		
	}

	//GETTER
	public int getId() {
		return id;
	}
	
	public String getTitre() {
		return titre;
	}
	
	public String getContenu() {
		return contenu;
	}
	
	public String getAuteur() {
		return auteur;
	}
	
	public Date getDate() {
		return date;
	}
	
	public String getTag() {
		return tag;
	}
	
	public int getIdUser() {
		return idUser;
	}
	
	public int getIdCategorie() {
		return idCategorie;
	}
	
	//SETTER
	public void setId(int id) {
		this.id = id;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	
	public void setContenu(String contenu) {
		this.contenu = contenu;
	}
	
	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public void setTag(String tag) {
		this.tag = tag;
	}
	
	public void setIdUser(int id_user) {
		this.idUser = id_user;
	}
	
	public void setIdCategorie(int id_categorie) {
		this.idCategorie = id_categorie;
	}
	





}
